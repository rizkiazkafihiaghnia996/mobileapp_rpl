package com.example.finalproject_rizkiazkafihiaghnia_bmrpl

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import com.example.finalproject_rizkiazkafihiaghnia_bmrpl.databinding.ActivityPasswordVerificationBinding
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException

class PasswordVerificationActivity : AppCompatActivity() {
    private lateinit var binding : ActivityPasswordVerificationBinding
    private lateinit var auth : FirebaseAuth
    private var mIsShowPass = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPasswordVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        binding.showPassword.setOnClickListener() {
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }

        binding.proceedButton.setOnClickListener() {
           val password = binding.passwordInput.text.toString()

           if (password.isEmpty()) {
               binding.passwordInput.error = "Please input your password"
               binding.passwordInput.requestFocus()
               return@setOnClickListener
           }

           user?.let {
               val userCredential = EmailAuthProvider.getCredential(it.email!!, password)
               it.reauthenticate(userCredential).addOnCompleteListener {
                   if (it.isSuccessful) {
                       Intent(this, EditProfileActivity::class.java).also {
                           it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                           startActivity(it)
                       }
                   }
                   else if (it.exception is FirebaseAuthInvalidCredentialsException) {
                       Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show()
                   }
                   else {
                       Toast.makeText(this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show()
                   }
               }
           }
        }

        binding.cancelButton.setOnClickListener() {
            Intent(this, BottomNavBarActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        }

        showPassword(mIsShowPass)
    }

    private fun showPassword(isShowed : Boolean) {
        if (isShowed) {
            binding.passwordInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            binding.showPassword.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        }
        else {
            binding.passwordInput.transformationMethod = PasswordTransformationMethod.getInstance()
            binding.showPassword.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        binding.passwordInput.setSelection(binding.passwordInput.text.toString().length)
    }
}

