package com.example.projekakhirmobileapp_rizkiazkafihiaghnia_bmrpl

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter2_Tugas: RecyclerView.Adapter<RecyclerAdapter2_Tugas.Viewholder2_Tugas>() {

    private var movieTitles2 = arrayOf(
        "No Time To Die",
        "Dune",
        "Shang Chi and...",
        "Venom: Let Th...",
        "Free Guy"
    )

    private val moviePosters = intArrayOf(
        R.drawable.notimetodie,
        R.drawable.dune,
        R.drawable.shangchi,
        R.drawable.lettherebecarnage,
        R.drawable.freeguy
    )

    private val movieRatings = arrayOf(
        "4 out of 5",
        "4.2 out of 5",
        "3.8 out of 5",
        "3.5 out of 5",
        "3.7 out of 5"
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter2_Tugas.Viewholder2_Tugas {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_layout2_tugas, parent, false)
        return Viewholder2_Tugas(v)
    }

    override fun onBindViewHolder(holder: RecyclerAdapter2_Tugas.Viewholder2_Tugas, position: Int) {
        holder.movieTitle2.text = movieTitles2[position]
        holder.moviePoster.setImageResource(moviePosters[position])
        holder.movieRating.text = movieRatings[position]
    }

    override fun getItemCount(): Int {
        return movieTitles2.size
    }

    inner class Viewholder2_Tugas(itemView: View): RecyclerView.ViewHolder(itemView) {
        var moviePoster: ImageView
        var movieTitle2: TextView
        var movieRating: TextView

        init {
            moviePoster = itemView.findViewById(R.id.movie_poster)
            movieTitle2 = itemView.findViewById(R.id.movie_title2)
            movieRating = itemView.findViewById(R.id.rating)

            itemView.setOnClickListener {
                val position: Int = adapterPosition

                Toast.makeText(
                    itemView.context,
                    "Error 503 - Service Unavalable: Server under maintenance, we really apologize for this inconvenience",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    }
}