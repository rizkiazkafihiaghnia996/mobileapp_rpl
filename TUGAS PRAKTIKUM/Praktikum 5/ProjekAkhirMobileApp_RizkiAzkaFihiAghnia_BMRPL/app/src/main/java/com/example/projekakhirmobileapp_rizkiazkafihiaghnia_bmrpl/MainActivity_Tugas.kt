package com.example.projekakhirmobileapp_rizkiazkafihiaghnia_bmrpl

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity_Tugas : AppCompatActivity() {

    private var layoutManager_tugas: RecyclerView.LayoutManager? = null
    private var adapter_tugas: RecyclerView.Adapter<RecyclerAdapter_Tugas.ViewHolder_Tugas>? = null
    private var layoutManager2_tugas: RecyclerView.LayoutManager? = null
    private var adapter2_tugas: RecyclerView.Adapter<RecyclerAdapter2_Tugas.Viewholder2_Tugas>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recyclerview_tugas)

        layoutManager_tugas = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val recyclerView_tugas = findViewById<RecyclerView>(R.id.recyclerViewTugas)
        recyclerView_tugas.layoutManager = layoutManager_tugas

        adapter_tugas = RecyclerAdapter_Tugas()
        recyclerView_tugas.adapter = adapter_tugas

        layoutManager2_tugas = LinearLayoutManager(this)
        val recyclerView2_tugas = findViewById<RecyclerView>(R.id.recyclerView2Tugas)
        recyclerView2_tugas.layoutManager = layoutManager2_tugas

        adapter2_tugas = RecyclerAdapter2_Tugas()
        recyclerView2_tugas.adapter = adapter2_tugas
    }
}