package com.example.finalproject_rizkiazkafihiaghnia_bmrpl

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.widget.Toast
import com.example.finalproject_rizkiazkafihiaghnia_bmrpl.databinding.ActivityEditProfileBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class EditProfileActivity : AppCompatActivity() {
    private lateinit var binding : ActivityEditProfileBinding
    private lateinit var auth : FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    var databaseFirebase : FirebaseDatabase? = null
    private var mIsShowPass = false
    private var mIsShowConfirmedPass = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = FirebaseAuth.getInstance()
        databaseFirebase = FirebaseDatabase.getInstance()
        databaseReference = databaseFirebase?.reference!!.child("profile")
        val user = auth.currentUser
        val currentUserDB = databaseReference.child((user?.uid!!))

        binding.eMailInput.setText(user?.email)
        binding.usernameInput.setText(currentUserDB.get().result?.child("username")?.value.toString())
        binding.phoneNumberInput.setText(currentUserDB.get().result?.child("phone")?.value.toString())

        binding.showPassword.setOnClickListener() {
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }

        binding.showConfirmPassword.setOnClickListener() {
            mIsShowConfirmedPass = !mIsShowConfirmedPass
            showConfirmPassword(mIsShowConfirmedPass)
        }

        binding.btnSaveProfile.setOnClickListener() {
            val username = binding.usernameInput.text.toString()
            val email = binding.eMailInput.text.toString()
            val password = binding.passwordInput.text.toString()
            val confirmedPassword = binding.confirmPasswordInput.text.toString()
            val phone = binding.phoneNumberInput.text.toString()

            val userData = mapOf<String, String>(
                "username" to username,
                "phone" to phone
            )

            if (username.isEmpty() || username.length < 6) {
                binding.usernameInput.error = "Username can't be less than 6 characters"
                binding.usernameInput.requestFocus()
                return@setOnClickListener
            }

            if (username.length > 13) {
                binding.usernameInput.error = "Username can't exceed 13 characters"
                binding.usernameInput.requestFocus()
                return@setOnClickListener
            }

            if (email.isEmpty()) {
                binding.eMailInput.error = "E-mail field can't be blank"
                binding.eMailInput.requestFocus()
                return@setOnClickListener
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                binding.eMailInput.error = "Provided e-mail is invalid"
                binding.eMailInput.requestFocus()
                return@setOnClickListener
            }

            if (password.isEmpty() || password.length < 6) {
                binding.passwordInput.error = "Password must contain 6 characters or longer"
                binding.passwordInput.requestFocus()
                return@setOnClickListener
            }

            if (confirmedPassword.isEmpty() || confirmedPassword != password) {
                binding.confirmPasswordInput.error = "Password wasn't confirmed"
                binding.confirmPasswordInput.requestFocus()
                return@setOnClickListener
            }

            user?.let {
                user.updateEmail(email).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Intent(this, BottomNavBarActivity::class.java).also {
                            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(it)
                        }
                    } else if (it.exception is FirebaseAuthUserCollisionException) {
                        Toast.makeText(this, "Email is already in used", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }

                user.updatePassword(password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Intent(this, BottomNavBarActivity::class.java).also {
                            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(it)
                        }
                    } else {
                        Toast.makeText(this,"${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }

                currentUserDB.updateChildren(userData).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Intent(this, BottomNavBarActivity::class.java).also {
                            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(it)
                        }
                    }
                    else {
                        Toast.makeText(this,"${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }


            }
        }

        binding.btnCancel.setOnClickListener() {
            Intent(this, BottomNavBarActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        }

        showPassword(mIsShowPass)
        showConfirmPassword(mIsShowConfirmedPass)
    }

    private fun showPassword(isShowed: Boolean) {
        if (isShowed) {
            binding.passwordInput.transformationMethod =
                HideReturnsTransformationMethod.getInstance()
            binding.showPassword.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        } else {
            binding.passwordInput.transformationMethod =
                PasswordTransformationMethod.getInstance()
            binding.showPassword.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        binding.passwordInput.setSelection(binding.passwordInput.text.toString().length)
    }

    private fun showConfirmPassword(isShowed: Boolean) {
        if (isShowed) {
            binding.confirmPasswordInput.transformationMethod =
                HideReturnsTransformationMethod.getInstance()
            binding.showConfirmPassword.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        } else {
            binding.confirmPasswordInput.transformationMethod =
                PasswordTransformationMethod.getInstance()
            binding.showConfirmPassword.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        binding.confirmPasswordInput.setSelection(binding.confirmPasswordInput.text.toString().length)
    }
}
