package com.example.finalproject_rizkiazkafihiaghnia_bmrpl

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton

class TermsAndConditionsActivity : AppCompatActivity() {
    private lateinit var continueButton: Button
    private lateinit var cancelButton: Button
    private lateinit var agreeCheckBox: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_termsandconditions)

        continueButton = findViewById(R.id.btn_continue)
        cancelButton = findViewById(R.id.btn_cancel)
        agreeCheckBox = findViewById(R.id.agree)

        continueButton.setOnClickListener() {
            intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        cancelButton.setOnClickListener() {
            intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        agreeCheckBox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener() {
            buttonView: CompoundButton?, isChecked: Boolean ->
            if (isChecked) {
                continueButton.setVisibility(View.VISIBLE)
            }
            else {
                continueButton.setVisibility(View.GONE)
            }
        })
    }
}