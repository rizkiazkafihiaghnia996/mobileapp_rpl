package com.example.finalproject_rizkiazkafihiaghnia_bmrpl

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.widget.*
import com.example.finalproject_rizkiazkafihiaghnia_bmrpl.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var birthDatePicker : TextView
    private lateinit var btnDatePicker: Button
    private lateinit var btnBack: ImageView
    private lateinit var passInput: EditText
    private lateinit var showPassButton: ImageButton
    private lateinit var confirmPassInput: EditText
    private lateinit var showConfirmPassButton: ImageButton
    private lateinit var binding : ActivityRegisterBinding
    private lateinit var auth : FirebaseAuth
    private lateinit var databaseReference: DatabaseReference
    var databaseFirebase : FirebaseDatabase? = null
    private var mIsShowPass = false
    private var mIsShowConfirmPass = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = FirebaseAuth.getInstance()
        databaseFirebase = FirebaseDatabase.getInstance()
        databaseReference = databaseFirebase?.reference!!.child("profile")


        binding.btnSignUp.setOnClickListener {
            val username = binding.usernameInput.text.toString()
            val email = binding.eMailInput.text.toString()
            val password = binding.passwordInput.text.toString()
            val confirmedPassword = binding.confirmPasswordInput.text.toString()
            val phoneNumber = binding.phoneNumberInput.text.toString()
            val birthDate = binding.btnDatePicker.text.toString()

            if (username.isEmpty() || username.length < 6) {
                binding.usernameInput.error = "Username can't be less than 6 characters"
                binding.usernameInput.requestFocus()
                return@setOnClickListener
            }

            if (username.length > 13) {
                binding.usernameInput.error = "Username can't exceed 13 characters"
                binding.usernameInput.requestFocus()
                return@setOnClickListener
            }

            if (email.isEmpty()) {
                binding.eMailInput.error = "E-mail field can't be blank"
                binding.eMailInput.requestFocus()
                return@setOnClickListener
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                binding.eMailInput.error = "Provided e-mail is invalid"
                binding.eMailInput.requestFocus()
                return@setOnClickListener
            }

            if (password.isEmpty() || password.length < 6) {
                binding.passwordInput.error = "Password must contain 6 characters or longer"
                binding.passwordInput.requestFocus()
                return@setOnClickListener
            }

            if (confirmedPassword.isEmpty() || confirmedPassword != password) {
                binding.confirmPasswordInput.error = "Password wasn't confirmed"
                binding.confirmPasswordInput.requestFocus()
                return@setOnClickListener
            }

            if (phoneNumber.isEmpty() || phoneNumber.length < 12) {
                binding.phoneNumberInput.error = "Phone number can't be less than 12 digits"
                binding.phoneNumberInput.requestFocus()
                return@setOnClickListener
            }

            if (phoneNumber.length > 13) {
                binding.phoneNumberInput.error = "Phone number can't exceed 13 digits"
                binding.phoneNumberInput.requestFocus()
                return@setOnClickListener
            }

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentUser = auth.currentUser
                    val currentUserDB = databaseReference.child((currentUser?.uid!!))
                    currentUserDB.child("username").setValue(username)
                    currentUserDB.child("phone").setValue(phoneNumber)
                    currentUserDB.child("dateofbirth").setValue(birthDate)
                    Intent(this, LoginActivity::class.java).also {
                        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(it)
                    }
                    Toast.makeText(this, "Account Created Successfully", Toast.LENGTH_SHORT).show()
                    finish()
                }
                else if (it.exception is FirebaseAuthUserCollisionException) {
                    Toast.makeText(this, "E-mail is already in used", Toast.LENGTH_SHORT).show()
                }
                else {
                    Toast.makeText(this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show()
                }
            }
        }

        birthDatePicker = findViewById(R.id.birth_date)
        btnDatePicker = findViewById(R.id.btn_datePicker)
        btnBack = findViewById(R.id.back)
        passInput = findViewById(R.id.password_input)
        showPassButton = findViewById(R.id.show_password)
        confirmPassInput = findViewById(R.id.confirm_password_input)
        showConfirmPassButton = findViewById(R.id.show_confirm_password)

        val myCalendar = Calendar.getInstance()

        val datePicker = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, month)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            updateLabel(myCalendar)
        }


        btnDatePicker.setOnClickListener {
            DatePickerDialog(this,
                datePicker,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH),
            ).show()

        }

        btnBack.setOnClickListener {
            intent = Intent(this, TermsAndConditionsActivity::class.java)
            startActivity(intent)
        }

        showPassButton.setOnClickListener() {
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }

        showConfirmPassButton.setOnClickListener() {
            mIsShowConfirmPass = !mIsShowConfirmPass
            showConfirmPassword(mIsShowConfirmPass)
        }

        showPassword(mIsShowPass)
        showConfirmPassword(mIsShowConfirmPass)

    }

    private fun updateLabel(myCalendar: Calendar) {
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        btnDatePicker.setText(sdf.format(myCalendar.time))
    }

    private fun showPassword(isShowed: Boolean) {
        if(isShowed) {
            passInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            showPassButton.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        }
        else {
            passInput.transformationMethod = PasswordTransformationMethod.getInstance()
            showPassButton.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        passInput.setSelection(passInput.text.toString().length)
    }

    private fun showConfirmPassword(isShowed: Boolean) {
        if(isShowed) {
            confirmPassInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            showConfirmPassButton.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        }
        else {
            confirmPassInput.transformationMethod = PasswordTransformationMethod.getInstance()
            showConfirmPassButton.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        confirmPassInput.setSelection(confirmPassInput.text.toString().length)
    }
}