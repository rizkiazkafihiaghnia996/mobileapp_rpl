package com.example.finalproject_rizkiazkafihiaghnia_bmrpl

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.finalproject_rizkiazkafihiaghnia_bmrpl.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class LoginActivity : AppCompatActivity() {
    private lateinit var  signInButton: Button
    private lateinit var signUpButton: Button
    private var mIsShowPass = false
    private lateinit var passInput: EditText
    private lateinit var showPassButton: ImageButton
    private lateinit var binding : ActivityLoginBinding
    private lateinit var auth : FirebaseAuth
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        signInButton = findViewById(R.id.sign_in_btn)
        signUpButton = findViewById(R.id.sign_up_btn)
        passInput = findViewById(R.id.password_input)
        showPassButton = findViewById(R.id.show_password)

        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser
        if (currentUser != null) {
            val intent = Intent(this, BottomNavBarActivity::class.java)
            startActivity(intent)
        }

        signInButton.setOnClickListener() {
            val email = binding.emailInput.text.toString()
            val password = binding.passwordInput.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                auth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Intent(this, BottomNavBarActivity::class.java).also {
                            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(it)
                            Toast.makeText(this, "You are logged in. Welcome" + email, Toast.LENGTH_SHORT).show()
                        }
                    }
                    else if (it.exception is FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(this, "Invalid e-mail or password", Toast.LENGTH_SHORT).show()
                    }
                    else if (it.exception is FirebaseAuthInvalidUserException) {
                        Toast.makeText(this, "No account found with the provided credentials", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        Toast.makeText(this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            else if (email.isEmpty()) {
                binding.emailInput.error = "Please input your e-mail"
                binding.emailInput.requestFocus()
                return@setOnClickListener
            }
            else if (password.isEmpty()) {
                binding.passwordInput.error = "Please input your password"
                binding.passwordInput.requestFocus()
                return@setOnClickListener
            }
        }

        signUpButton.setOnClickListener() {
            intent = Intent(this, TermsAndConditionsActivity::class.java)
            startActivity(intent)
        }

        showPassButton.setOnClickListener() {
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }

        showPassword(mIsShowPass)

        binding.forgotPassword.setOnClickListener() {
            intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showPassword(isShowed: Boolean) {
        if(isShowed) {
            passInput.transformationMethod = HideReturnsTransformationMethod.getInstance()
            showPassButton.setImageResource(R.drawable.ic_baseline_visibility_off_24)
        }
        else {
            passInput.transformationMethod = PasswordTransformationMethod.getInstance()
            showPassButton.setImageResource(R.drawable.ic_baseline_visibility_24)
        }
        passInput.setSelection(passInput.text.toString().length)
    }
}