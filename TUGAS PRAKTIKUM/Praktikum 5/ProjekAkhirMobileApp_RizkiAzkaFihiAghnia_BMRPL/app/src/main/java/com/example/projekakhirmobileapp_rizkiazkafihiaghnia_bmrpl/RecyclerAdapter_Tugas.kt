package com.example.projekakhirmobileapp_rizkiazkafihiaghnia_bmrpl

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter_Tugas: RecyclerView.Adapter<RecyclerAdapter_Tugas.ViewHolder_Tugas>() {

    private var movieTitles1 = arrayOf(
        "The Batman",
        "The Flash",
        "Spider-Man: No Way Home",
        "Black Adam",
        "Doctor Strange: Multiverse of Madness"
    )

    private val movieBanners = intArrayOf(
        R.drawable.batman,
        R.drawable.theflash,
        R.drawable.nowayhome,
        R.drawable.blackadam,
        R.drawable.multiverseofmadness
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter_Tugas.ViewHolder_Tugas {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_layout_tugas, parent, false)
        return ViewHolder_Tugas(v)
    }

    override fun onBindViewHolder(holder: RecyclerAdapter_Tugas.ViewHolder_Tugas, position: Int) {
        holder.movieTitle1.text = movieTitles1[position]
        holder.movieBanner.setImageResource(movieBanners[position])
    }

    override fun getItemCount(): Int {
        return movieTitles1.size
    }

    inner class ViewHolder_Tugas(itemView: View): RecyclerView.ViewHolder(itemView) {
        var movieBanner: ImageView
        var movieTitle1: TextView

        init {
            movieBanner = itemView.findViewById(R.id.movie_banner)
            movieTitle1 = itemView.findViewById(R.id.movie_title1)

            itemView.setOnClickListener {
                val position: Int = adapterPosition

                Toast.makeText(
                    itemView.context,
                    "Error 503 - Service Unavalable: Server under maintenance, we really apologize for this inconvenience",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}